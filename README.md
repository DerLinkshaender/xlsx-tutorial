# xlsx-tutorial

## _Reading and writing xlsx files with Go_

This repo provides a short tutorial for the Golang xlsx package created by Geoff Teale 
(available under https://github.com/tealeg/xlsx)

The tutorial is written in AsciiDoc for processing by https://AsciiDoctor.org and currently this is a work in progress with the goal to learn more about Go and writing documentation in AsciiDoc.
